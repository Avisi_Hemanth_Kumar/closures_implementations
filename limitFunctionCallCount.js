export let limitFunctionCallCount = function (cb, n) {
    let count = 0;
    if (n <= 0)
        return null;
    return () => {
        if (count < n) {
            count += 1;
            return cb();
        }
    }
}

