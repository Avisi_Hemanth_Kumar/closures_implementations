export let counterFactory = function () {
    let value = 0;

    return {
        increment: function () {
            value += 1;
            return value;
        },
        decrement: function () {
            value -= 1;
            return value;
        }
    };
};