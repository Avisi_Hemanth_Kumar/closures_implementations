import {
    counterFactory
} from '/home/hemanth/MountBlue/closures/counterFactory.js';

console.log("Initial value of counter1 is 0");
console.log("Initial value of counter2 is 0");
console.log("Initial value of counter3 is 0 \n");

let counter1 = counterFactory();
let counter2 = counterFactory();
let counter3 = counterFactory();


console.log("Counter1 Incremnet : " + counter1.increment());
console.log("Counter2 decremnet : " + counter2.decrement());
console.log("Counter3 Incremnet : " + counter3.increment() + '\n');


console.log("Counter1 decremnet : " + counter1.decrement());
console.log("Counter2 decremnet : " + counter2.decrement());
console.log("Counter3 decremnet : " + counter3.decrement());