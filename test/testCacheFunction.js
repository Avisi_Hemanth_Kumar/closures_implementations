import {
    cacheFunction
} from '/home/hemanth/MountBlue/closures/cacheFunction.js';
let testcacheFunction = (cacheFunction(function (x) {
    console.log(x + '\n')
}));

testcacheFunction(7);
testcacheFunction(7);
testcacheFunction(8);
testcacheFunction(6);
testcacheFunction(6);