import {
    limitFunctionCallCount
} from '/home/hemanth/MountBlue/closures/limitFunctionCallCount.js';

let testLimitFunctionCallCount = (limitFunctionCallCount(function () {
    console.log('Hii\n')
}, 3));

testLimitFunctionCallCount();
testLimitFunctionCallCount();
testLimitFunctionCallCount();